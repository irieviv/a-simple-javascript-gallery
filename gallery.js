function changeGalleryBackground(color)
{
	document.getElementById('galleryContainer').style.backgroundColor = color;
}

var imgs = [
			['Photos/how-to-achieve-success.jpg', 0],
			['Photos/imPOSSIBLE.jpg', 1],
			['Photos/on-top-of-the-mountain.jpg', 0],
			['Photos/success-just-ahead.jpg', 1],
			['Photos/the-iceberg-of-success.jpg', 0],
			['Photos/cat.jpg', 0]
		   ];

var imgGallery = document.getElementById('imgGallery');

var current = 0;
imgGallery.setAttribute('src', imgs[current][0]);
changeStar(imgs[current][1]);

function prevImg()
{
	current--;
	if(current < 0)
	{
		current = imgs.length - 1;
	}
	imgGallery.setAttribute('src', imgs[current][0]);
	changeStar(imgs[current][1]);
}

function addImg()
{
	var newImg = prompt('Add image:', 'https://upload.wikimedia.org/wikipedia/commons/e/e1/FullMoon2010.jpg');
	if(newImg === "")// user pressed OK, but the input field was empty
	{
		alert('This field is required!');
	}
	else if(newImg)// user typed something and hit OK
	{
		imgs.push([newImg, 0]);
	}
	else// user hit cancel
	{
		alert('Input was cancelled.');
	}
}

function nextImg()
{
	current++;
	if(current > imgs.length - 1)
	{
		current = 0;
	}
	imgGallery.setAttribute('src', imgs[current][0]);
	changeStar(imgs[current][1]);
}

function likes()
{
	if(imgs[current][1] == 0)
	{
		imgs[current][1] = 1;
		changeStar(imgs[current][1]);
	}
	else
	{
		imgs[current][1] = 0;
		changeStar(imgs[current][1]);
	}
}

function changeStar(arrElem)
{
	var star = document.getElementById('star');
	if(arrElem == 0)
	{
		star.setAttribute('src', 'Photos/star.png');
	}
	else
	{
		star.setAttribute('src', 'Photos/star-liked.png');
	}
}